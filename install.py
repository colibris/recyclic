#!/bin/env python3
"""
This will install YesWiki on a YunoHost server
It will also add the lms plugin to it
This script must run as root
"""
import argparse
import importlib.util
import json
import os
import subprocess
import sys
from typing import Any
from typing import Dict
from typing import List
from typing import Optional

import domain
import utils
from arguments import Arguments


def main() -> None:
    parser = argparse.ArgumentParser(description="Install YNH apps from a config file")
    parser.add_argument(
        "-d",
        "--dryrun",
        action="store_true",
        help="Do not run the commands, print them instead",
    )
    parser.add_argument(
        "--admin",
        help="Admin username to use for all apps, overrides file specs",
    )
    parser.add_argument(
        "-f",
        "--file",
        default="list.json",
        help="JSON file with the config",
    )
    args = parser.parse_args()

    if not ran_as_root():
        print("Please run this script as root, using sudo for example")
        exit(2)

    try:
        with open("list.json", "r") as f:
            install_list_unverified = json.load(f)
    except FileNotFoundError:
        print("Error: The FILE argument should be a readable file")
        parser.print_usage()
        exit(1)
    except json.decoder.JSONDecodeError as e:
        print("Error: The provided FILE must be valid JSON")
        print(e)
        parser.print_usage()
        exit(1)

    install_list = validate_params(install_list_unverified)

    for application in install_list:
        if args.admin:
            application["arguments"]["admin"] = args.admin
        install(application, args.dryrun)
        run_extensions(application, args.dryrun)

    # Only run postinstall scripts once all apps have been installed
    for application in install_list:
        run_postinstall_script(application, args.dryrun)


def ran_as_root() -> bool:
    return os.geteuid() == 0


def validate_params(install_list) -> List[Dict[str, Any]]:
    assert isinstance(install_list, list)
    for application in install_list:
        assert isinstance(application, dict)
        if "application" in application:
            if not isinstance(application["application"], str):
                raise ValueError('The "application" key must be an application name')
        else:
            raise ValueError('The "application" key must exist for each application')

        app_id = application["application"]

        if "arguments" in application:
            if not isinstance(application["arguments"], dict):
                raise ValueError(
                    f'The "arguments" key of app "{app_id}" must be a mapping of arguments and values'
                )
        else:
            raise ValueError(f'The "arguments" key of app "{app_id}" must exist')

        if "url" in application:
            if not (
                isinstance(application["url"], str)
                and application["url"][:8] == "https://"
            ):
                raise ValueError(f'The "url" key of app "{app_id}" must be an URL')

        if "postInstallScript" in application:
            pi_script = application["postInstallScript"]
            if not (isinstance(pi_script, str) and os.path.isfile(pi_script)):
                raise ValueError(
                    f'The "postInstallScript" key of app "{app_id}" must be an executable file'
                )

        if "extensions" in application:
            if not isinstance(application["extensions"], dict):
                raise ValueError(
                    f'The "extensions" key of app "{app_id}" must be a list of extensions names and parameters'
                )

    return install_list


def install(application: Dict[str, Any], dryrun: bool = False):
    app_args = Arguments(application["application"], application["arguments"])

    check_domain(app_args["domain"], dryrun)

    if "url" in application and application["url"]:
        app_handle = application["url"]
    else:
        app_handle = application["application"]

    app_id = existing_app(
        application["application"], app_args["domain"], app_args["path"]
    )
    if app_id:
        command = ["yunohost", "app", "upgrade", app_id]
        if app_handle != application["application"]:
            command.append("-u")
            command.append(app_handle)
    else:
        command = [
            "yunohost",
            "app",
            "install",
            app_handle,
            "-f",
            "-a",
            app_args.format(),
        ]

    if dryrun:
        print(command)
    else:
        res = subprocess.run(command)

        if res.returncode != 0:
            print(
                'Error installing "'
                + application["application"]
                + '" with command "'
                + " ".join(command)
                + '".'
            )
            print("Received exit code " + str(res.returncode))
            exit(res.returncode)

        apps_list = get_apps_list()
        app = utils.last_updated(apps_list)
        application["settings"] = app["settings"]

    if "default" in application and application["default"]:
        print(f'Setting {application["application"]} as default')
        makedefault(application, dryrun)


def get_apps_list() -> list:
    """Get the installed app list"""
    info = subprocess.run(
        ["yunohost", "--output-as", "json", "app", "list", "-f"],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        text=True,
    )
    return json.loads(info.stdout)["apps"]


def existing_app(
    app_name: str, app_domain: str, app_path: Optional[str] = None
) -> Optional[str]:
    """Tries to get an install id for the app with name, domain and path identical"""
    apps = get_apps_list()
    found = None

    for app in apps:
        if app["manifest"]["id"] == app_name:
            # If app is found, check if it's multi_instance and has a domain
            if app["manifest"]["multi_instance"] and "domain" in app["settings"]:
                # If it is, check if path and domain are the same
                if "path" in app["settings"] and app_path is not None:
                    if (
                        app_domain == app["settings"]["domain"]
                        and app_path == app["settings"]["path"]
                    ):
                        found = app
                        break
                # If it's not, only check for the domain
                else:
                    if app_domain == app["settings"]["domain"]:
                        found = app
                        break
            else:
                found = app
                break

    if found is not None:
        return found["id"]
    return None


def check_domain(app_domain: str, dryrun: bool = False):
    if dryrun:
        if not domain.exists(app_domain):
            print(
                f"Domain {app_domain} will be created, and a Let's Encrypt certificate requested for it"
            )
        elif not domain.has_letsencrypt(app_domain):
            print(f"A Let's Encrypt certificate for {app_domain} will be created")
    else:
        if not domain.create_if_not_exists(app_domain):
            print(f"Please create domain {app_domain} before installing")
            exit(1)
        elif not domain.setup_letsencrypt(app_domain):
            print(
                f"You will need to setup Let's Encrypt manually for {app_domain} to remove TLS certificate warnings"
            )


def makedefault(application: Dict[str, Any], dryrun: bool = False):
    if not dryrun:
        makedefault_res = subprocess.run(
            ["yunohost", "app", "makedefault", application["settings"]["id"]]
        )
        if makedefault_res.returncode != 0:
            print('Error setting "' + application["application"] + '" as default.')
            print("Received exit code " + str(makedefault_res.returncode))
            exit(makedefault_res.returncode)


def run_extensions(application: Dict[str, Any], dryrun: bool = False):
    if "extensions" not in application:
        return

    for name, params in application["extensions"].items():
        module_name = "extensions." + application["application"] + "." + name

        spec = importlib.util.find_spec(module_name)
        if spec is not None and spec.loader is not None:
            # Performing the actual import ...
            module = importlib.util.module_from_spec(spec)
            spec.loader.exec_module(module)  # type: ignore[attr-defined]
            sys.modules[module_name] = module
        else:
            print(
                'Error running extensions for "'
                + application["application"]
                + "\": can't find the "
                + module_name
                + " module"
            )
            exit(1)

        try:
            module.run(application, params, dryrun)  # type: ignore[attr-defined]  # Parameter dependent module
        except AttributeError:
            print(
                'Error running extensions for "'
                + application["application"]
                + '": module '
                + module_name
                + " has no function run"
            )
            exit(1)
        except TypeError:
            print(
                'Error running extensions for "'
                + application["application"]
                + '": function '
                + module_name
                + ".run does not have the proper signature"
            )
            exit(1)


def run_postinstall_script(application: Dict[str, Any], dryrun: bool = False):
    if "postInstallScript" in application:
        if dryrun:
            print(application["postInstallScript"])
        else:
            res = subprocess.run(application["postInstallScript"])

        if res.returncode != 0:
            print(
                'Error running postInstall for "'
                + application["application"]
                + '" with command "'
                + application["postInstallScript"]
                + '".'
            )
            print("Received exit code " + str(res.returncode))
            exit(res.returncode)


if __name__ == "__main__":
    main()
