"""Installs the specified YesWiki plugins

The plugins must be available in the public YesWiki repository

They can be specified as a list of plugins, and the latest versions will be used
Or as a table of `"plugin": "version"` and the specific versions will be used

Examples:
```json
"plugins": {
    "lms": "2021-04-06-25",
    "qrcode": "latest"
}
```

```json
"plugins": [
    "lms",
    "qrcode"
]
```
"""
import subprocess
from pathlib import Path
from typing import Union

from . import repository
from ..utils import run_as_user


def run(application: dict, plugins: Union[list, dict], dryrun: bool) -> None:
    if dryrun:
        print(__name__, plugins)
    else:
        final_path = Path(application["settings"]["final_path"])
        ynh_app_id = application["settings"]["id"]
        with run_as_user(ynh_app_id):
            for name in plugins:
                if isinstance(plugins, dict):
                    version = plugins.get(name, "latest")
                else:
                    version = "latest"
                print(f"Installing plugin {name}")
                if repository.download(
                    final_path, repository.Types.PLUGIN, name, version
                ):
                    update(final_path)


def update(path: Path):
    print("Running postupdate")
    subprocess.run(
        [
            "php7.3",
            path / "tools/autoupdate/commands/console",
            "updater:postupdate",
            "-n",
        ],
        cwd=path,
    )
